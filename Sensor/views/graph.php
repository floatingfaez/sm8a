
<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12 mb-3">

                <div class="d-flex gap-5 mb-5">
<!--                    <button class="btn btn-secondary" id="dropdownMenuButton" type="button">Add Graph</button>-->
                </div>

                <div class="table-responsive">
                    <table class="table table-striped graph-table">
                        <tr>
                            <th>Sensor Graphs</th>
                            <th>Sensor Date Added</th>
                            <th>Location</th>
                            <th>Display Name</th>
                            <th>Graph Category</th>
                            <th>Status</th>
                            <th>Graph Setting</th>
                        </tr>
                        <tr>
                            <td>Refrigerator 1</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Refrigerator Readings</td>
                            <td>Refrigerator</td>
                            <td><span class="badge text-bg-primary px-4">Active</span></td>
                            <td>
                                <div class="d-flex">
                                    <a data-val="1" class="btn btn-success me-2 Graph-Configuration-Graph-show-btn" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>Freezer 1</td>
                            <td>2016/04/01</td>
                            <td>BOH</td>
                            <td>Freezer Reading</td>
                            <td>Freezer</td>
                            <td><span class="badge text-bg-danger px-4">Battery Low</span></td>
                            <td>
                                <div class="d-flex">
                                    <a data-val="2" class="btn btn-success me-2 Graph-Configuration-Graph-show-btn" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>Bain Marie Hot</td>
                            <td>2019/07/01</td>
                            <td>Loading Dock</td>
                            <td>Main Hot Buffet</td>
                            <td>Refrigerator</td>
                            <td><span class="badge text-bg-secondary px-4">Not Responding</span></td>
                            <td>
                                <div class="d-flex">
                                    <a data-val="3"  class="btn btn-success me-2 Graph-Configuration-Graph-show-btn" href="#" >
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>

                    </table>
                </div>

                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link text-info" href="#">Prev</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">1</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">2</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">3</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">Next</a></li>
                    </ul>
                </nav>

            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->
        <div class="card bg-chart mb-4 Graph-Configuration-Graph-area d-none">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <h4 class="card-title mb-0"> <span class="title-name"></span> - Last 7 Days</h4>
                        <div class="small text-medium-emphasis time-date">November 2017</div>
                    </div>
                </div>
                <div class="c-chart-wrapper" style="height:300px;margin-top:40px;" id="chartContainer">
                    <canvas class="chart" id="myChart"  width="400" height="150"></canvas>
                </div>


            </div>
            <div class="card-footer">
                <div class="row row-cols-1 row-cols-md-5 text-center">
                    <div class="col mb-sm-2 mb-0" id="Daily_Temp_Average">
                        <div class="text-medium-emphasis">Daily Temp Average</div>
                        <div class="fw-semibold">
                            <span class="user"></span> Users (<span class="percentage"></span>)
                        </div>
                        <div class="progress progress-thin mt-2">

                        </div>
                    </div>

                    <div class="col mb-sm-2 mb-0" id="Weekly_Temp_Average">
                        <div class="text-medium-emphasis">Weekly Temp Average</div>
                        <div class="fw-semibold">
                            <span class="user"></span> Users (<span class="percentage"></span>)
                        </div>
                        <div class="progress progress-thin mt-2">

                        </div>
                    </div>



                    <div class="col mb-sm-2 mb-0" id="Total_Reads_Received">
                        <div class="text-medium-emphasis">Total Reads Received</div>
                        <div class="fw-semibold">
                            <span class="user"></span> Users (<span class="percentage"></span>)
                        </div>
                        <div class="progress progress-thin mt-2">

                        </div>
                    </div>



                    <div class="col mb-sm-2 mb-0" id="Average_Battery_Voltage">
                        <div class="text-medium-emphasis">Average Battery Voltage</div>
                        <div class="fw-semibold">
                            <span class="user"></span> Users (<span class="percentage"></span>)
                        </div>
                        <div class="progress progress-thin mt-2">

                        </div>
                    </div>





                    <div class="col mb-sm-2 mb-0" id="Total_Missed_Reads">
                        <div class="text-medium-emphasis">Total Missed Reads</div>
                        <div class="fw-semibold">
                            <span class="percentage"></span>
                        </div>
                        <div class="progress progress-thin mt-2">

                        </div>
                    </div>





                </div>
            </div>
        </div>
        <!-- /.card.mb-4-->
    </div>
</div>








