<?php
namespace Sensor\Controllers;

use Sensor\Abstracts\Controller;



class SensorController {
    public function __construct(){}

    public static function init() {
        $shortcodes = array(
            'sensors' => array(__CLASS__, 'index'),
        );

        foreach ($shortcodes as $shortcode => $function) {
            add_shortcode($shortcode, $function);
        }
    }

    public static function index($atts) {
        $base = new Controller();
        return $base->load_view($atts,'sensors');
    }


}
