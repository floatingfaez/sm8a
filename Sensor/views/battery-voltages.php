
<div class="body flex-grow-1 px-3">
    <div class="container-lg">

        <div class="row">
            <div class="col-12 mb-4">

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Sensor ID</th>
                            <th>Battery Replaced On</th>
                            <th>Location</th>
                            <th>Display Name</th>
                            <th>Battery Voltage</th>
                            <th>Sensor Setting</th>
                        </tr>
                        <tr>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td><span class="badge text-bg-primary px-4">3.85</span></td>

                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td><span class="badge text-bg-danger px-4">3.85</span></td>

                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td><span class="badge text-bg-secondary px-4">3.85</span></td>

                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td><span class="badge text-bg-warning px-4">3.85</span></td>

                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td><span class="badge text-bg-success px-4">3.85</span></td>

                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                    </table>
                </div>

                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link text-info" href="#">Prev</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">1</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">2</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">3</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">Next</a></li>
                    </ul>
                </nav>

            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->

        <div class="card mb-4">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <h4 class="card-title mb-0">Sensor Battery Voltage</h4>
                        <div class="small text-medium-emphasis">November 2017</div>
                    </div>
                </div>
                <div class="c-chart-wrapper" style="height:300px;margin-top:40px;">
                    <canvas class="chart" id="main-chart" height="300"></canvas>
                </div>
            </div>
            <div class="card-footer">
                <div class="row row-cols-1 row-cols-md-5 text-center">
                    <div class="col mb-sm-2 mb-0">
                        <div class="text-medium-emphasis">Daily Temp Average</div>
                        <div class="fw-semibold">29.703 Users (40%)</div>
                        <div class="progress progress-thin mt-2">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col mb-sm-2 mb-0">
                        <div class="text-medium-emphasis">Weekly Temp Average</div>
                        <div class="fw-semibold">24.093 Users (20%)</div>
                        <div class="progress progress-thin mt-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col mb-sm-2 mb-0">
                        <div class="text-medium-emphasis">Total Reads Received</div>
                        <div class="fw-semibold">78.706 Views (60%)</div>
                        <div class="progress progress-thin mt-2">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col mb-sm-2 mb-0">
                        <div class="text-medium-emphasis">Average Battery Voltage</div>
                        <div class="fw-semibold">22.123 Users (80%)</div>
                        <div class="progress progress-thin mt-2">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col mb-sm-2 mb-0">
                        <div class="text-medium-emphasis">Total Missed Reads</div>
                        <div class="fw-semibold">40.15%</div>
                        <div class="progress progress-thin mt-2">
                            <div class="progress-bar" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card.mb-4-->
    </div>
</div>
