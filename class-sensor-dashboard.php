<?php
defined( 'ABSPATH' ) || exit;

use Sensor\Controllers\GraphController;
use Sensor\Controllers\BatteryVoltageController;
use Sensor\Controllers\SensorController;

class Sensor_Dashboard {

    protected static $instance;

    protected $admin;

    private $ajax;

    protected $integrations;

    private $graphController;

    public function __construct() {
        $this->init_plugin();
    }

    public function init_plugin() {
        $this->includes();
        $this->init_hooks();
    }

    public function init_hooks(){
        GraphController::init();
        BatteryVoltageController::init();
        SensorController::init();
    }

    public function includes() {
        if ( is_admin() ) {
            add_action( 'admin_init', array($this,'sensor_admin_init') );
        }else{
            $this->load_class( 'class-sensor-frontend.php', 'Sensor_Frontend' );
        }
    }

    public function load_class( $local_path, $class_name ) {
        require_once( SM8A_SENSOR_ROOT .'/'. $local_path );
        return new $class_name;
    }

    public function sensor_admin_init() {
        $this->init_routes();
//        $this->admin = $this->load_class( '/admin/class-sensor-admin.php', SENSOR_TEXT_DOMAIN );
    }

    private function is_page_exists($post_name) {
        return !empty(get_page_by_path($post_name));
    }

    private function sensor_create_pots($name, $type, $content){
        $post_data = array(
            'post_title'    => wp_strip_all_tags( $name ),
            'post_content'  => $content,
            'post_status'   => 'publish',
            'post_type'     => $type,
            'post_author'   => get_current_user_id(),
            'page_template' => 'full_width_no_header_footer.php'
        );
        wp_insert_post($post_data);
    }

    protected function init_routes() {
        if (!$this->is_page_exists('graph-configuration')) {
            $this->sensor_create_pots('Graph Configuration', 'page', '[sensor_graph]');
        }
        if (!$this->is_page_exists('battery-voltages')) {
            $this->sensor_create_pots('Battery Voltages', 'page', '[sensor_battery_voltage]');
        }
        if (!$this->is_page_exists('sensors')) {
            $this->sensor_create_pots('Sensors', 'page', '[sensors]');
        }
    }

    public static function instance() {

        if ( null === self::$instance ) {

            self::$instance = new self();
        }

        return self::$instance;
    }

}


function init_sensor_db_instanse() {
    return \Sensor_Dashboard::instance();
}
