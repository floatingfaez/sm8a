<?php
defined( 'ABSPATH' ) || exit;

use Sensor\Controllers\GraphController;

class Sensor_Shortcodes{
    public static function init(){
        $shortcodes = array(
            'sensor_graph' => 'GraphController::index',
        );

        foreach ($shortcodes as $shortcode => $function) {
            add_shortcode($shortcode, $function);
        }
    }

}
