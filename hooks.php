<?php
    defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );
    // Admin Hooks
    add_action('osf_users_role_managment_hook','osf_libraries_styles');
    add_action('osf_users_role_managment_hook','add_jquery_only');
    add_action('osf_users_role_managment_hook','osf_libraries_scripts');

    add_action('resource_managment_hook','osf_libraries_styles');
    add_action('resource_managment_hook','add_jquery_only');
    add_action('resource_managment_hook','osf_libraries_scripts');

    add_action('tables_dropdown_list_hook','osf_libraries_styles');
    add_action('tables_dropdown_list_hook','add_jquery_only');
    add_action('tables_dropdown_list_hook','osf_libraries_scripts');

    add_action('disable_user_managment_hook','osf_libraries_styles');
    add_action('disable_user_managment_hook','add_jquery_only');
    add_action('disable_user_managment_hook','osf_libraries_scripts');

    add_action('disable_role_hook','osf_libraries_styles');
    add_action('disable_role_hook','add_jquery_only');
    add_action('disable_role_hook','osf_libraries_scripts');

    add_action('membership_managment_hook','osf_libraries_styles');
    add_action('membership_managment_hook','add_jquery_only');
    add_action('membership_managment_hook','osf_libraries_scripts');

    add_action('set_password_user_hook','osf_libraries_styles');
    add_action('set_password_user_hook','add_jquery_only');
    add_action('set_password_user_hook','osf_libraries_scripts');

    add_action('account_check_hook','osf_libraries_styles');
    add_action('account_check_hook','add_jquery_only');
    add_action('account_check_hook','osf_libraries_scripts');

    add_action('duplicate_user_hook','osf_libraries_styles');
    add_action('duplicate_user_hook','add_jquery_only');
    add_action('duplicate_user_hook','osf_libraries_scripts');

    add_action('duplicate_membership_hook','osf_libraries_styles');
    add_action('duplicate_membership_hook','add_jquery_only');
    add_action('duplicate_membership_hook','osf_libraries_scripts');

    add_action('default_email_template_hook','osf_libraries_styles');
    add_action('default_email_template_hook','add_jquery_only');
    add_action('default_email_template_hook','osf_libraries_scripts');

    add_action('page_builder_hook','osf_libraries_styles');
    add_action('page_builder_hook','add_jquery_only');
    add_action('page_builder_hook','osf_libraries_scripts');

    add_action('authorised_workers_hook','osf_libraries_styles');
    add_action('authorised_workers_hook','add_jquery_only');
    add_action('authorised_workers_hook','osf_libraries_scripts');

    add_action('limited_access_file_hook','osf_libraries_styles');
    add_action('limited_access_file_hook','add_jquery_only');
    add_action('limited_access_file_hook','osf_libraries_scripts');

    // Loing Page
    add_action('osf_login_hook', 'add_jquery_only' );
    add_action('osf_login_hook','osf_libraries_scripts');

    add_filter('wp_headers','osf_libraries_styles');
    add_filter('theme_footer_box_hook','osf_libraries_scripts');

    add_action('osf_complete_form_view_port_hook','osf_libraries_styles');
    add_action('osf_complete_form_view_port_hook','osf_libraries_scripts');

    add_filter('login_redirect','redirect_to_homepage' );
    add_filter('logout_redirect','redirect_to_homepage' );

    add_action('osf_complete_form_view_port_hook_check_membership','redirect_to_homepage' );

    add_filter('show_admin_bar', '__return_false' );

    add_action('init','invitation_approved_link');

    add_filter( 'cron_schedules', 'osf_abn_cron_set_time' );

    //Create schedules for send Cancel ABN email
    add_action ('abn_cancelled_or_missing', 'osf_abn_cancel_or_missing_cron_callback');
    if (!wp_next_scheduled('abn_cancelled_or_missing')) {
        wp_schedule_event( time(), 'sm8a_fortnightly', 'abn_cancelled_or_missing' );
    }

    //Create schedules for update ABN results
    add_action ('update_abn_from_gov_site_to_database', 'osf_abn_update_cron_callback');
    if (!wp_next_scheduled('update_abn_from_gov_site_to_database')) {
        wp_schedule_event( time(), 'sm8a_monthly', 'update_abn_from_gov_site_to_database' );
    }

    //Create schedules for Summary of Compliance Portal
    add_action ('summary_of_compliance_portal', 'summary_of_compliance_portal_callback');
    if (!wp_next_scheduled('summary_of_compliance_portal')) {
        wp_schedule_event( time(), 'sm8a_monthly', 'summary_of_compliance_portal' );
    }
