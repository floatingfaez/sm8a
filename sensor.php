<?php

/**
 * @link             expressosoft.com
 * @since             1.0.0
 * @package           sensor-dashboard
 *
 * @wordpress-plugin
 * Plugin Name:       Sensor Dashboard
 * Plugin URI:        https://www.floatingfaez.com
 * Description:       Sensor data storing and visualization
 * Version:           1.0.1
 * Author:            Floating Faez
 * Author URI:        https://www.floatingfaez.com
 * License:           UNLICENSED
 * License URI:       UNLICENSED
 * Text Domain:       sensor-dashboard
 * Domain Path:       /languages
 */

defined( 'ABSPATH' ) || exit;

define('SM8A_SENSOR_VERSION', '1.0.1');
define('SM8A_SENSOR_ROOT', dirname(__FILE__));
define('SM8A_SENSOR_ROOT_URL', plugin_dir_url(__FILE__));
define('SM8A_SENSOR_ASSET_URL', SM8A_SENSOR_ROOT_URL.'sensor/assets');
define('SENSOR_TEXT_DOMAIN', 'sensor-dashboard');


require_once __DIR__ . '/vendor/autoload.php';

class Sensor_Loader {
    private static $instance;

    protected function __construct() {
        $this->init_plugin();
    }

    private function init_plugin() {
        register_activation_hook(__FILE__, array( $this,'sensor_dashboard_activation'));
        register_deactivation_hook(__FILE__, array( $this,'sensor_dashboard_deactivation'));

        require_once( SM8A_SENSOR_ROOT . '/class-sensor-dashboard.php' );
        init_sensor_db_instanse();
    }

    public function sensor_dashboard_activation(){

    }

    public function sensor_dashboard_deactivation(){

    }

    public static function instance() {

        if ( null === self::$instance ) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}

Sensor_Loader::instance();
