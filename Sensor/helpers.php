<?php



function load_sensor_scripts(){
    $sensor_styles = array('style', 'custome',);
    $sensor_vendor_styles = array('coreui-chartjs');

    $sensor_scripts = array('main');
    $sensor_vendor_scripts = array(
        '@coreui/coreui/js/coreui.bundle.min',
        'simplebar/js/simplebar.min',
        'chart.js/js/chart.min',
        '@coreui/chartjs/js/coreui-chartjs',
        '@coreui/utils/js/coreui-utils',
    );

    foreach ($sensor_styles as $style){
        wp_enqueue_style( 'sensor-'.$style, SM8A_SENSOR_ASSET_URL.'/css/'.$style.'.css', array(),'1.0','all');
    }
    foreach ($sensor_vendor_styles as $style){
        wp_enqueue_style( 'sensor-vendor-'.$style, SM8A_SENSOR_ASSET_URL.'/vendors/@coreui/chartjs/css/'.$style.'.css', array(),'1.0','all');
    }
    foreach ($sensor_vendor_scripts as $script){
        $script_name = str_replace('.min', '',end(explode('/',$script)));
        wp_enqueue_script( 'sensor-vendor-'.$script_name, SM8A_SENSOR_ASSET_URL.'/vendors/'.$script.'.js', array('jquery'),'1.0',true);
    }
    foreach ($sensor_scripts as $script){
        wp_enqueue_script( 'sensor-'.$script, SM8A_SENSOR_ASSET_URL.'/js/'.$script.'.js', array('jquery'),'1.0',true);
    }

    global $post;
    if($post->post_name === 'graph-configuration'){
        wp_enqueue_script( 'sensor-graph-conf', SM8A_SENSOR_ASSET_URL.'/js/graph-conf.js', array('jquery'),'1.0',true);
    }
    if($post->post_name === 'battery-voltages'){
        wp_enqueue_script( 'sensor-battery-voltages', SM8A_SENSOR_ASSET_URL.'/js/battery-voltage.js', array('jquery'),'1.0',true);
    }
    if($post->post_name === 'sensors'){
        wp_enqueue_script( 'sensors', SM8A_SENSOR_ASSET_URL.'/js/sensors.js', array('jquery'),'1.0',true);
    }
}


function sensor_load_view($name, $data = []){
  extract($data);
  require_once SM8A_SENSOR_ROOT . "/Sensor/views/{$name}.php";
}


function sm8a_sensor_title_case($string) {
  return str_replace('_', ' ', ucwords($string, '_'));
}


