


jQuery(document).ready(function($) {

    let filteredObjects=[];

    let canvasReset =()=>{

    };










    let drawGraph=(graph_data)=>{

        canvasReset()
        let DataSet=[]
        $.each(graph_data, function(index, item) {
            DataSet.push({
                label: item.label,
                borderColor:eval(item.borderColor) ,
                pointHoverBackgroundColor: item.pointHoverBackgroundColor,
                data :item.data,
                fill: item.fill
            })
        });

        let chartStatus = Chart.getChart("myChart");
        if (chartStatus != undefined) {
            chartStatus.destroy();
        }


        var chartCanvas = $('#myChart');
        chartInstance = new Chart(chartCanvas, {
            type: 'line',
            data   : {
                labels  : ['M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S',],
                datasets:DataSet,
            },
            options: {
                maintainAspectRatio: false,
                plugins            : {
                    legend: {
                        display: false
                    }
                },
                scales             : {
                    x: {
                        grid: {
                            drawOnChartArea: false
                        }
                    },
                    y: {
                        ticks: {
                            beginAtZero  : true,
                            maxTicksLimit: 5,
                            stepSize     : Math.ceil(250 / 5),
                            max          : 250
                        }
                    }
                },
                elements           : {
                    line : {
                        tension: 0.4
                    },
                    point: {
                        radius          : 0,
                        hitRadius       : 10,
                        hoverRadius     : 4,
                        hoverBorderWidth: 3
                    }
                }
            }
        });


    }


    let getGraphData=(buttonId)=>{
        $.ajax({
            url: '../wp-content/plugins/Sensor-Dashboard/Sensor/assets/fakeDb/graph.json', // Replace with the actual path to your JSON file
            dataType: 'json',
            success: function(data) {
                filteredObjects = $.grep(data, function(object) {
                    return object.id == buttonId;
                });
                setGraph(filteredObjects)
            },
            error: function() {
                console.log('Failed to retrieve JSON data.');
            }
        });
    }


    let setGraph=(filteredObjects)=>{
       drawGraph(filteredObjects[0].graph_data)



        $('.Graph-Configuration-Graph-area .title-name').text(filteredObjects[0].Display_Name)
        $('.Graph-Configuration-Graph-area .time-date').text(convertDateString(filteredObjects[0].Sensor_Date_Added))


        $('#Daily_Temp_Average .user').html(filteredObjects[0].Daily_Temp_Average.user)
        $('#Daily_Temp_Average .percentage').html(filteredObjects[0].Daily_Temp_Average.percentage)
        $('#Daily_Temp_Average .progress').html(`
         <div class="progress-bar bg-success" role="progressbar" style="width:${filteredObjects[0].Daily_Temp_Average.percentage}" aria-valuenow="40"
                                 aria-valuemin="0" aria-valuemax="100"></div>
        `)





        $('#Weekly_Temp_Average .user').html(filteredObjects[0].Weekly_Temp_Average.user)
        $('#Weekly_Temp_Average .percentage').html(filteredObjects[0].Weekly_Temp_Average.percentage)
        $('#Weekly_Temp_Average .progress').html(`
         <div class="progress-bar bg-info" role="progressbar" style="width:${filteredObjects[0].Weekly_Temp_Average.percentage}" aria-valuenow="40"
                                 aria-valuemin="0" aria-valuemax="100"></div>
        `)




        $('#Total_Reads_Received .user').html(filteredObjects[0].Total_Reads_Received.user)
        $('#Total_Reads_Received .percentage').html(filteredObjects[0].Total_Reads_Received.percentage)
        $('#Total_Reads_Received .progress').html(`
         <div class="progress-bar bg-warning" role="progressbar" style="width:${filteredObjects[0].Total_Reads_Received.percentage}" aria-valuenow="40"
                                 aria-valuemin="0" aria-valuemax="100"></div>
        `)




        $('#Average_Battery_Voltage .user').html(filteredObjects[0].Average_Battery_Voltage.user)
        $('#Average_Battery_Voltage .percentage').html(filteredObjects[0].Average_Battery_Voltage.percentage)
        $('#Average_Battery_Voltage .progress').html(`
         <div class="progress-bar bg-danger" role="progressbar" style="width:${filteredObjects[0].Average_Battery_Voltage.percentage}" aria-valuenow="40"
                                 aria-valuemin="0" aria-valuemax="100"></div>
        `)



        $('#Total_Missed_Reads .percentage').html(filteredObjects[0].Total_Missed_Reads.percentage)
        $('#Total_Missed_Reads .progress').html(`
         <div class="progress-bar" role="progressbar" style="width:${filteredObjects[0].Total_Missed_Reads.percentage}" aria-valuenow="40"
                                 aria-valuemin="0" aria-valuemax="100"></div>
        `)








    }



    $('.Graph-Configuration-Graph-show-btn').on('click',function () {
        getGraphData($(this).attr('data-val'));
        $('.Graph-Configuration-Graph-area').removeClass('d-none')
    })










    let convertDateString=(dateString)=>{
        const dateParts = dateString.split('/');
        const year = parseInt(dateParts[0]);
        const month = parseInt(dateParts[1]);
        const day = parseInt(dateParts[2]);
        const date = new Date(year, month - 1, day);
        const monthName = date.toLocaleString('en-US', { month: 'long' });
        const formattedYear = date.toLocaleString('en-US', { year: 'numeric' });
        const formattedDate = `${monthName} ${formattedYear}`;
        return formattedDate;
    }










});

