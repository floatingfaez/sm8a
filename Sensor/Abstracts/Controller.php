<?php
namespace Sensor\Abstracts;

class Controller{
    public function load_view($atts, $view){
        $atts = (array) $atts;
        load_sensor_scripts();
        ob_start();
        do_action('compliance_init');
        theme_header_box();
            sensor_load_view($view);
        theme_footer_box();
        return ob_get_clean();
    }

}
