<div class="body flex-grow-1 px-3">
    <div class="container-lg">
        <div class="row">
            <div class="col-12 mb-5">

                <div class="d-flex justify-content-between mb-5">
                    <div class="dropdown">
                        <button class="btn btn-secondary" id="dropdownMenuButton" type="button" data-coreui-toggle="modal" data-coreui-target="#add-sensor-modal">Add Sensor</button>
                        <button class="btn btn-secondary" id="dropdownMenuButton2" type="button" data-coreui-toggle="modal" data-coreui-target="#archive-sensor-modal">Archive Sensor</button>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped sensor-table with-checkbox-column">
                        <tr>
                            <th>
                                <div class="form-check">
                                    <input class="form-check-input check-all" type="checkbox" value="" />
                                </div>
                            </th>
                            <th>Sensor ID</th>
                            <th>Sensor Date Added</th>
                            <th>Location</th>
                            <th>Display Name</th>
                            <th>Sensor Category</th>
                            <th>Status</th>
                            <th>Sensor Setting</th>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check-individual" type="checkbox" value=""/>
                                </div>
                            </td>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td>Refrigerator</td>
                            <td><span class="badge text-bg-primary px-4">Active</span></td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check-individual" type="checkbox" value=""/>
                                </div>
                            </td>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td>Freezer</td>
                            <td><span class="badge text-bg-danger px-4">Battery Low</span></td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check-individual" type="checkbox" value=""/>
                                </div>
                            </td>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td>Refrigerator</td>
                            <td><span class="badge text-bg-secondary px-4">Not Responding</span></td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check-individual" type="checkbox" value=""/>
                                </div>
                            </td>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td>Refrigerator</td>
                            <td><span class="badge text-bg-warning px-4">Temp Increase</span></td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check-individual" type="checkbox" value=""/>
                                </div>
                            </td>
                            <td>93508345</td>
                            <td>2012/01/01</td>
                            <td>BOH</td>
                            <td>Main Cold Room</td>
                            <td>Bain Marle</td>
                            <td><span class="badge text-bg-success px-4">Active</span></td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-success me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-search"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-info me-2" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                        </svg>
                                    </a>
                                    <a class="btn btn-danger" href="#">
                                        <svg class="icon text-white">
                                            <use xlink:href="<?php echo SM8A_SENSOR_ASSET_URL;?>/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                        </svg>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link text-info" href="#">Prev</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">1</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">2</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">3</a></li>
                        <li class="page-item"><a class="page-link text-info" href="#">Next</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-lg-6 col-xxl-4 mb-5">
                <div class="dropdown mb-4">
                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton3" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Temperature Notification Settings</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" style="">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Enter Minimum Temperature">
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Enter Maximum Temperature">
                </div>
                <div class="input-group mb-5">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Enter Minutes Out-of-Range">
                </div>
                <div class="dropdown mb-4">
                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton4" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Temperature Notification Settings</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" style="">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Group Name">
                </div>
            </div>

            <div class="col-lg-6 col-xxl-4 mb-5">
                <div class="dropdown mb-4">
                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton5" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Sensor Notification Schedule</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" style="">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Enter 7 Day Schedule">
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Enter Allowed Times for Notifications">
                </div>
                <div class="input-group mb-5">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Enter Enter Minimum Temperature">
                </div>
                <div class="dropdown mb-4">
                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton6" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Sensor Group Notification Settings</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" style="">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Group Name">
                </div>
            </div>

            <div class="col-lg-6 col-xxl-4 mb-5">
                <div class="dropdown mb-4">
                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton7" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Sensor % Over Temp (Forced Notification)</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" style="">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </div>
                <div class="input-group mb-3">
                    <button class="btn btn-info text-white dropdown-toggle" type="button" data-coreui-toggle="dropdown" aria-expanded="false">Action</button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    <input type="text" class="form-control" placeholder="Enter % Warning for SMS/Email/In-App">
                </div>
                <p>Sensor % Over Temp warnings ignore sensor notification schedule</p>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->
        <!-- /.card.mb-4-->
    </div>
</div>

<div class="modal fade" id="add-sensor-modal" tabindex="-1"  aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Sensor</h5>
                <button type="button" class="btn-close" data-coreui-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row mb-3">
                        <label for="inputEmail3" class="col-sm-4 col-form-label">Sensor ID</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputEmail3" >

                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="location" class="col-sm-4 col-form-label">Location</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="location" >

                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="displayName" class="col-sm-4 col-form-label">Display Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="displayName">

                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="category" class="col-sm-4 col-form-label">Sensor Category</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="category" >

                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="group" class="col-sm-4 col-form-label">Sensor Group</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="group" >

                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="notification" class="col-sm-4 col-form-label">Sensor Notifications Enable/Disable</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="notification">

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="archive-sensor-modal" tabindex="-1"  aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Archive Sensors</h5>
                <button type="button" class="btn-close" data-coreui-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3>Do you want to archive selected sensor?</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">No</button>
                <button type="button" class="btn btn-danger" style="color:#fff ">Yes</button>
            </div>
        </div>
    </div>
</div>
