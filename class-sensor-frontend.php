<?PHP

defined( 'ABSPATH' ) || exit;

class Sensor_Frontend {

    public function __construct(){
        $this->frontend_initialization();
    }

    private function frontend_initialization(){
//        $this->init_hooks();
        $this->init_rest_api_handler();
    }

    private function init_hooks(){
        include_once (SM8A_SENSOR_ROOT.'/hooks.php');
    }

    protected function init_rest_api_handler() {

    }

}
